// Package github provides a GitHub API
package github

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"
)

// IssuesURL is the GitHub API url
const IssuesURL = "https://api.github.com/search/issues"

// IssuesSearchResult is the model for a search result
type IssuesSearchResult struct {
	TotalCount int `json:"totalCount"`
	Items      []*Issue
}

// Issue is the model for a GitHub issue
type Issue struct {
	Number    int
	HTMLURL   string `json:"htmlUrl"`
	Title     string
	State     string
	User      *User
	CreatedAt time.Time `json:"createdAt"`
	Body      string
}

// User is the model for a single user
type User struct {
	Login   string
	HTMLURL string `json:"htmlUrl"`
}

// SearchIssues queries the GitHub issue tracker
func SearchIssues(terms []string) (*IssuesSearchResult, error) {
	query := url.QueryEscape(strings.Join(terms, " "))
	res, err := http.Get(IssuesURL + "?q=" + query)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		res.Body.Close()
		return nil, fmt.Errorf("search query faild: %s", res.Status)
	}

	var response IssuesSearchResult
	// streaming decoder
	if err := json.NewDecoder(res.Body).Decode(&response); err != nil {
		res.Body.Close()
		return nil, err
	}

	res.Body.Close()
	return &response, nil
}
