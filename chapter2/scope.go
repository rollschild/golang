package main

import "fmt"

func testScope() {
	var testInt int = 6
	fmt.Println(testInt)
}

func main() {
	// NOT gonna work
	fmt.Println(testInt)
}
