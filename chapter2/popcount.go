package main

import "fmt"

var pc [256]byte

func init() {
	for i := range pc {
		pc[i] = pc[i/2] + byte(i&1)
	}
}

func popcount(num uint64) int {
	return int(
		pc[byte(num>>(0*8))] +
			pc[byte(num>>(1*8))] +
			pc[byte(num>>(2*8))] +
			pc[byte(num>>(3*8))] +
			pc[byte(num>>(4*8))] +
			pc[byte(num>>(5*8))] +
			pc[byte(num>>(6*8))] +
			pc[byte(num>>(7*8))])
}

func main() {
	var testNum uint64 = 11
	fmt.Println(popcount(testNum))
}
