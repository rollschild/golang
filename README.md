# The Go Programming Language
- _call by value_ parameter passing
- _communicating sequential processes_
  - a program is a parallel composition of processes that have no shared state
  - processes communicate and synchronize using **channels**
- Go code is organized into _packages_
  - each source file begins with a `package` declaration
  - `package main` is special: it defines a standalone executable program, _not_ a library
- for the most part, order of declarations does **not** matter
- `os.Args`: command-line arguments
  - a **slice** of strings
  - `os.Args[0]` is the command itself
  - slice of all arguments: `os.Args[1:len(os.Args)]` or `os.Args[1:]`
- `:=` - short variable declaration
  - declare variables and give them appropriate types based on the initializer values
- `x++` and `x--` are **statements**, **not** expressions
  - no return value
  - `x = i++` is **ILLEGAL**
  - _postfix only_
- the `for` loop is the _only_ loop in Go
  - the initialization part is optional
  - if present, it must be a _simple statement_
- Go does **NOT** permit unused local variables
- the short variable declaration `s := ""` may be used **only** within a function
  - not for package-level variables
- `strings.Join()`
- `range` over `map` produces `key, value` pair
  - the order of iteration is **random**
- Read from input:
  - the `bufio` package
  - the `Scanner` type
  - `input := bufio.NewScanner(os.Stdin)`
  - each call to `input.Scan()` reads the next line and removes the newline at the end
    - return `true` if there's a line and `false` if there's no more input
  - use `input.Text()` to retrieve the content
- `fmt.Printf()` - formatted output
  - does **not** write a newline by default
- `os.Open()` opens a new file and returns 
  - `f`: file handle
  - `err`: error of built-in `error` type
- `fmt.Fprintf()` prints to the default standard error stream
- a _map_ is a reference to what `make` allocates
  - when passing a map to a function,
  - _a copy of the reference_ is passed
- `ioutil.ReadFile()` returns a _byte_ slice
- `const`
  - value of a constant must be a number, string, or boolean
- `[]someSlice{...}` & `someStruct{...}`: composite literal
  - quickly instantiate any of Go's composite types
- simple http server
  - request is a `struct` of type `http.Request`
  - `http.ListenAndServe(addr, handler)` starts an http server with the given address and handler
    - handler normally would be `nil` - using the default `DefaultServeMux`
  - `http.HandleFunc(patter, handler)` registers the `handler` function for the given patter
  - `http.ResponseWriter` -> web browser
    - of _interface_ `io.Writer`
- `switch`
  - cases do **NOT** fall through
  - there's a rarely used `fallthrough` statement
  - does **NOT** need a condition/operand - tagless
- statements may be labeled
  - ...so that `break` and `continue` can refer to them
- NO POINTER ARITHMETIC
- variable declared in a function is **local**
  - if declared outside of a function, it's visible throughout all files in that package
- the case of the first letter of a variable -> determines its visiblilty across pacakges
  - upper case: variable is exported and visible outside of the package
- short names for local variables with small scopes
- `var name type = expression`
  - either `type` or `expression` can be omitted
  - but **not** both
- _zero types_:
  - `""` for `string`
  - `nil` for `interface` and reference types (slice, map, channel, func)
- _short variable declaration_ can only be used in a function, not on package level
  - in other words, local variables
- swap the values of two variables: `i, j = j, i`
  - `a[i], a[j] = a[j], a[i]`
- not every value has an address, but every variable does
- zero value of a pointer is `nil`
- pointers are comparable - two pointers are equal if and only if 
  - they point to the same variable
  - or, both are `nil`
- `new(T)`: create an unnamed variable of type `T` -> return its address
  - so the variable itself is of type `*T`
- variable that escapes requires extra memory allocation
- the lifetime of a variable is determined _only_ by whether or not it's reachable
- Type declaration
  - `type <name> <underlying-type>`
  - most common on package level
  - once declared, they are different types, even though they have the same underlying type
  - **cannot** be compared or combined in arithmetic expressions
  - you need to use explicit type conversion
  - the ype conversion between two named types does **NOT** change the value, just its type
- A type conversion is allowed if:
  - both have the same underlying type
  - or, both are unnamed pointer types that point to variables of the same underlying type
- _a conversion never fails at runtime_???
- doc comment: immediately precedes the `package` declaration
  - only one file in each package should have a package doc comment
  - extensive documentation should go to `doc.go`
- Package Initialization
  - for some types such as tables of data, an initializer expression may not be the simplest
  - we use the `init` function mechanism
  - the `init` function cannot be called or referenced
  - `init` functions are automatically executed when the program starts
  - `func init() { /* ... */ }`
  - any file may contain any number of `init` functions
- Scope
  - vs. lifetime
  - scope is a region of program text; compile-time property
  - lifetime is a range of time during execution when the variable can be referred to by others
    - run-time property
  - imported packages such as `fmt` are at _file level_
- Lexical block:
  - also includes other groupings of declarations that are not explicitly surrounded by braces:
  - lexical block for the entire source code: _universe block_
  - the `for` loop creates two lexical blocks:
    - the explicit block for the loop body
    - an implicit block that additionally encloses the variables declared by the initialization clause
  - for `if` and `switch` statements
    - there's a block for the condition **and** a block for each case body
````
var cwd string

func init() {
    cwd, err := os.Getwd()
    if err != nil {
        log.Fatalf("os.Getwd() failed: %v", err)
    }
}
````
  - here, the `:=` statement declares both `cwd` and `err` as local
  - the outer `cwd` on package level is never initialized/touched/updated
  - below is better:
````
var cwd string

func init() {
    var err error
    cwd, err = os.Getwd()
    if err != nil {
        log.Fatalf("os.Getwd() failed: %v", err)
    }
}
````
- `log.Fatalf` prints a message and calls `os.Exit(1)`
- Basic data types
  - basic types
  - aggregate types
  - reference types
  - interface types
- `int` and `uint` are the two most natural/efficient size for a paticular platform
- type `rune` === `int32` - a **Unicode** code point
- `byte` === `uint8` - a piece of raw data, **not** a small numeric quantity
- `uintptr`: an unsigned integer type
  - its width is not specified but is sufficient to hold all bits of a **pointer** value
  - see the `unsafe` package
- `int` is not the same type as `int32`, even if the natural size of an integer is 32 bits
  - you need explicit conversion between them
- sign of remainder is always the same as that of the dividend
  - `-5%3 = -2`
  - `-5%-3 = -2`
- integer division truncates toward zero
  - `9/2 = 4` **not** `4.5` or `5`
- all values of basic type are comparable - including strings
- `&^`: AND NOT - bit clear
````
var i int8 = 32
fmt.Println(i<<2) // -128
````
- left shifts fill the vacated bits with `0`, 
  - as do right shifts of unsigned numbers
  - but right shifts of signed numbers fill the vacated bits with copies of the sign bit
- Generally we want to use the signed `int` form
  - we use unsigned number only when their bitwise operators or _peculiar_ airthmetic operators are required
- use `Printf`'s `%g` to print floating points
- `math.Exp()`
- `math.IsNaN` & `math.NaN`
  - `nan := math.NaN()`
  - **Any** comparison with NaN **always** yields `false` (except `!=`)
- Complex numbers
  - `complex64` and `complex128`, with components `float32` and `float64` respectively
  - `complex(real, imag)`
  - `real()` & `imag()`
  - use the `math/cmplx` package
  - `math.cmplx.Sqrt(complexNumber)`
- Booleans
  - _short_circuit_ behavior: `s != "" && s[0] == 'x'`
  - _itob_: `func itob(i int) bool { return i != 0 }`
- String
  - **Immutable** sequence of bytes
  - `len()` returns number of bytes (**not** runes)
  - `s[i]` returns the i-th **byte**
    - the i-th byte of the string is **not** necessarily the i-th character
    - UTF-8 encoding of some non-ASCII code requires more bytes
  - `s[i:j]` yields a new string of `j-i` bytes
  - `s[:]` - all bytes - a copy of the string
  - string values are immutable - bytes in a string can never be changed
  - but we can assign a new value to the string variable, or append extra bytes to the string
  - `"\xhh"` - two hex digits `h`
  - `"\ooo"` - three octal digits, not exceeding `\377`
- Raw string literal: ``...``
  - within raw literal, no escape is processed
  - carriage return is deleted
  - good at writing regex
- Unicode
  - naturally, `int32` holds a single `rune`
  - UTF-8 is a variable length encoding of Unicode points as bytes
  - it uses **between** 1 and 4 bytes to represent each rune,
    - but only 1 byte for ASCII
    - 2 or 3 bytes for most common runes
  - high-order bits of the first byte indicates how many bytes to follow
  - use package `unicode/utf8`
  - `\uhhhh` for 16-bit value and `\Uhhhhhhhh` for 32-bit value
````
func HasPrefix(s, prefix string) bool {
    return len(s) > len(prefix) && s[:len(prefix)] == prefix
}
````
  - `utf8.RuneCountInString()`
  - Unicode replacement character `'\uFFFD'`
  - `[]rune` applies to a UTF-8 encoded string and returns the sequence of Unicode points
  - to convert a slice of `rune` to string: `string(r)`
  - `[]byte(stringName)`
- Convert integer to string:
  - `y := fmt.Sprintf("%d", x)`
  - `strconv.Itoa(x)` - _integer to ASCII_
  - `strconv.FormatInt()` and `strconv.FormatUint()` format numbers in a different base
    - `strconv.FormatInt(int64(x), 2)`
  - better using `%b`, `%d`, `%x`, and `%o`
- Convert string to integer:
  - `strconv.Atoi(someString)`
  - `strconv.ParseInt(someString, 10, 64)` - base 10, up to 64 bits
    - the third argument refers to the size the result must fit into, **NOT** the actual int type
    - the type will always be `int64`
- Constants
  - value is known to the compiler; evaluation occurred at compile time, **NOT** runtime
  - must be basic type: boolean, string, or number
  - the constant generator `iota`
    - begins at zero
    - increments by one
````
const (
    _ = 1 << (10 * iota)
    KiB
    MiB
    GiB
    TiB
)
````
  - Untyped constants
    - not committed to a particular type
    - _but_ with higher precisions and can be involved in more expressions/operations
    - `true` and `false` are untyped booleans
    - string literals are untyped strings
    - only constants can be untyped
    - `var i = int8(0)` === `var i int8 = 0`
- Array
  - Fixed length
  - you may specify a list of index and value pairs
  - `r := [...]int{99: -1}`
    - defines an array with 100 elements, all zero except for the last, which is `-1`
  - the size of an array is part of its type
  - if an array's element type is comparable then the array type is comparable
  - when passing an array to a function, use its pointer!
- Slice
  - variable length
  - type of `[]T`, where elements all have type `T`
  - looks like an array without a size
  - a slice has an _underlying array_
  - has three components: pointer, length, and capacity
    - the pointer points to the first element of the array that's reachable through the slice
    - length is the number of slice elements; **cannot** exceed the capacity
    - capacity is the number of elements between the start of the slice and the end of the underlying array
  - multiple slices can share the same underlying array
  - _slice operator_: `s[i:j]`
  - slicing beyond `cap(s)` will panic, but slicing beyond `len(s)` extends the slice
  - passing a slice to a function permits the function to modify the underlying array elements
````
// reverse a slice of ints in place
func reverse(s []int) {
    for i, j := 0, len(s) - 1; i < j; i, j = i + 1, j - 1 {
        s[i], s[j] = s[j], s[i]
    }
}
````
  - slices are **NOT** comparable
  - for slices of bytes, we can use `bytes.Equal()`
  - the zero value of a slice type is `nil`, which has no underlying array
    - `[]int(nil)`
  - `make([]T, len)`
    - this slice is a view of the entire array
  - `make([]T, len, cap)` === `make([]T, cap)[:len]`
    - under the hood, make creates an unnamed array variableand returns a slice of it
    - this slice is a view of only the first `len` elements, but its capacity is the entire array
- Map
  - map is a reference to a hash table
  - type is `map[K]V`
  - all the keys are of the same type
  - all the values are same type
  - the key `K` must be comparable using `==`
  - do **NOT** use floating-point numbers as keys
  - `map[string]int{}` === `make(map[string]int)`
  - `delete(mapName, key)`
  - a map element is **NOT** a variable
  - you **CANNOT** take a map element's address
  - the zero value of a map is `nil`
  - storing to a `nil` map will panic!
  - you must allocate the mpa before you store to it
  - subscripting a map yields two values: the _value_ and a boolean
  - maps are **not** comparable, except with `nil`
- Struct
  - can store arbitrary types
  - each value is a _field_
  - you **CAN** take the address of a field
  - field order matters! it's significant to type identity!
  - a `struct` field is exported if it begins with a capital letter
    - struct may contain mixture of exported and unexported fields
  - a named struct type `S` cannot declare a field of the same type `S`
    - an aggregate value cannot contain itself
    - but `S` can declare a field of the pointer type `*S`
      - useful for linked lists and trees
  - larger struct types are usually passed to or returned from functions indirectly using a pointer
  - `pp = &Point{1, 2}` - create and initialize a struct and obtain its address
  - Comparison: if all the fields of a struct is comparable, then the struct itself is comparable
    - comparable struct types can be used as the key type of a map
  - _Struct Embedding_
    - one named struct type as a field of another struct type
    - even _anonymous fields_: a field with no name
      - this field must be a named type or a pointer to a named type
      - need **NOT** be `struct` types
      - _embedded_
    - **NO** anonymous fields shorthands for struct literals
    - but anonymous fields **DO** have names - you cannot have two anonymous fields of the same type
- JSON
  - _marshaling_: convert a Go data structure to JSON
  - **ONLY** exported fields are marshaled
  - **field tags**: a string of metadata associated at compile time with the field
  - _Unmarshaling_: JSON -> Go data structure
    - define your own output format
- Text and HTML Templates
  - `text/template` & `html/template`
  - _actions_: enclosed in `{{...}}` within a template
  - `{{.TotalCount}}` - the dot refers to the template's paramter
    - for this one it refers to `github.IssuesSearchResult`
  - `{{range .Items}}{{end}}` - a loop
  - within an action, the `|` makes the result of one operation the argument of another
    - pipeline
  - need to parse the template first; but only once
- Function
````
func sub(x, y int) (z int) {
    z = x - y
    return
}
````
  - _signature_: the type of a function
  - functions have the same type/signature if 
    - they have the same sequence of paramter types and
    - they have the same sequence of result types
  - Go has **NO** concept of default paramter values
  - Go has **NO** way to specify arguments by name
  - **ARGUMENTS ARE PASSED BY VALUE**
  - a function declaration without a body: this function is implemented in a language other than Go
  -
- Multiple return values
  - in a function with named results, the operands of a return statement may be omitted
    - it's called a _bare return_
- Errors
  - two types of errors that get returned:
    - `bool`, usually called `ok`
    - of type `error`
  - `error` can be `nil` or non-`nil`
    - call `error.Error` to get the `string` message
  - `fmt.Errorf()` formats an error message using `fmt.Sprintf()` and returns a new `error` value
  - error message strings should **NOT** be capitalized and newlines should be avoided
  - in general, the call `f(x)` is responsible for reporting the attempted operation `f` and the argument value `x` as they relate to the context of the error
  - `log.Fatalf()` is better than `os.Exit(1)`
  - `log.SetPrefix()` & `log.SetFlags()`
  - all `log` functions append a newline
  - successful logics should be minimally indented - at the outer level, not in the `else` block where the `if` one checks for `error`
  - `io.EOF`
- Function Values
  - functions are _first class values_ in Go
  - function values have types
  - zero value of a function type is `nil`
  - function values may be compared with `nil`
    - but **NOT** against each other
  - `strings.Map()`
  - `%*s` prints a string padded with a variable number of spaces
- Named functions can **only** be declared at the package level
  - but we can use a _function literal_ to denote a function value within any
      expression
  - a function literal is a `func` without a name
  - it's an expression
  - its value is called _anonymous function_
  - function values can have state
  - **closure**s
- Variadic Functions
  - the type of the final paramter is preceded by `...`
  - `func sum(vals ...int) int {}`
    - `vals` is a slice of `int`
  - implicitly, the caller allocates an array, copies the arguments into it, and
      passses a slice of the entire array to the function
  - to spread a slice: `sliceName...`
- `strings.HasPrefix(stringVal, testString)`
- `defer`
  - **any** number of calls may be deferred
    - they are executed in the **reverse** order in which they were deferred
  - the function and argument expressions are evaluated when the statement is
      executed, but the actual call is _deferred_ until the function that
      contains the `defer` statement has finished
  - the right place for a `defer` that releases a resource is immediately after
      the resource has been successfully acquired
  - deferred function runs **after** return statement has updated the funtion's
      result variables
    - thus deferred anonymous functions can observe the funtion's results
````
func double(x int) (result int) {
    defer func() {fmt.Printf("double(%d) = %d\n", x, result)}()
    return x + x
}
````
  - a `deferred` anonymous function can even change the values that the
      enclosing function returns to its caller:
````
func triple(x int) (result int) {
    defer func() {result += x}()
    return double(x)
}
fmt.Println(triple(4)) // 12
````
  - pay extra attention to the `defer` statement in  a loop
- Panic
  - during a typical panic, 
    - normal execution stops,
    - all deferred function calls in that goroutine are executed, in reverse
        order
    - the program crashes with a log message
      - the message includes the _panic value_ and the _stack trace_
  - **NOT ALL** panics happen at run time
    - the built-in `panic` function may be called directly
    - it accepts any value as an argument
    - often times `panic` is the best thing to do when some "_impossible_"
        situation happens
  - Opinion: any crash should be considered as a bug in the code
    - "_expected_" errors, such as failing I/O, should be handled gracefully
    - ...by using `error` values
  - the panic mechanism runs the deferred functions **before** it unwinds the
      stack
  - you can dump the stack by using the `runtime` package
    - `n := runtime.Stack(buf[:], false)`
- Recover
  - built-in `recover` function
  - if `recover` is called within a deferred function, and the function
      containing the `defer` statement is panicking,
    - `recover` ends the current state of panic and returns the panic value
  - Recovering indiscriminately from panics may **NOT** be a good idea
  - As a general rule, you **should not** attempt to recover from another
      package's panic
  - Public APIs should report failures as `error`s
  - it's safest to recover selectively if at all
- Methods
  - an _object_ is a value or variable that has methods
  - a _method_ is a function associated with a particular type
  - an object-oriented program is one that uses methods to express the
      properties and operations of each data structure
    - so that clients need not access the object's representation directly
  - _encapsulation_ and _composition_
  - choose the first letter of the type name as the name of the receiver
  - `p.methodName()` - a selector
  - Each type has its own namespace for methods
  - basically methods are allowed to be associated with any type
  - In a realistic program, convention is:
    - if any method of a type has a pointer receiver, then **all** methods of
        that type should have a pointer receiver
  - you **cannot** take address of a `struct` literal
  - `nil` is as valid receiver value
  - _method value_ and _method expression_
    - method value: `p.methodName`
    - method expression: `TypeName.methodName`
    - for method expression, when calling it, the first parameter is the
        receiver
````
p := Point{1, 2}
q := Point{3, 4}
distance := Point.Distance
distValue := distance(p, q)
````
# Interfaces
- interfaces are satisfied implicitly
- Concrete Type
  - specifies the exact representation of its values
  - exposes the intrinsic operations of that representation
- Interface Type
  - an _abstract_ type
  - _substitutability_
- a `String` method satisfies one of the most widely used interface,
    `fmt.Stringer`
- You can declare new interface types as combinations of existing ones
  - you _embed_ an interface into a new one
- a type _satisfies_ an interface if it possesses all the methods the interface
    requires
- an expression may be assigned to an interface only if its type satisfies the
    interface
- Interface Values
- _dynamic type_ and _dynamic value_
- in an interface value, the type component is represented by the appropriate
    type descriptor
- the zero value for an interface has both its type and value components set to
    `nil`
- 

