package main

import (
	"bytes"
	"fmt"
)

func printInts(ints []int) string {
	var buf bytes.Buffer
	buf.WriteString("[")

	for i, v := range ints {
		if i > 0 {
			buf.WriteString(", ")
		}
		fmt.Fprintf(&buf, "%d", v)
	}

	buf.WriteString("]")
	return buf.String()
}

func main() {
	fmt.Println(printInts([]int{1, 2, 3, 4, 5, 6, 7}))
}
