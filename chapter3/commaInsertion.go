package main

import "fmt"

func comma(s string) string {
	l := len(s)
	if l <= 3 {
		return s
	}

	return comma(s[:l-3]) + "," + s[l-3:]
}

func main() {
	str := "23432449876"

	fmt.Println(comma(str))
}
