package main

import (
	"fmt"
	"strings"
)

func main() {
	s := "a/b/c.basename.go"
	fmt.Println(basename(s))
}

func basename(s string) string {
	slashIndex := strings.LastIndex(s, "/")
	s = s[slashIndex+1:]

	if dotIndex := strings.LastIndex(s, "."); dotIndex >= 0 {
		s = s[:dotIndex]
	}

	return s
}
