package main

import (
	"fmt"
	"os"
)

func main() {
	var str string

	for index, arg := range os.Args[1:] {
		if index == len(os.Args)-1 {
			str += arg
		} else {
			str += arg + " "
		}
		// a fresh copy of string assigned to str every loop
	}

	fmt.Println(str)
}
