package main

import (
	"fmt"
	"os"
)

func main() {
	var str string

	for i := 1; i < len(os.Args); i++ {
		if i == len(os.Args)-1 {
			str += os.Args[i]
		} else {
			str += os.Args[i] + " "
		}
	}

	fmt.Println(str)
}
