package main

import "fmt"

type Values map[string][]string

func (v *Values) Get(key string) string {
	if val := (*v)[key]; len(val) > 0 {
		return val[0]
	}
	return ""
}

func main() {
	vals := &Values{"lang": {"en", "ru", "cn"}}
	str := vals.Get("lang")
	fmt.Println(str)
}
