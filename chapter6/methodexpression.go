package main

import "fmt"

type Point struct{ X, Y float64 }

func (p *Point) Add(q *Point) *Point {
	return &Point{p.X + q.X, p.Y + q.Y}
}
func (p *Point) Sub(q *Point) *Point {
	return &Point{p.X - q.X, p.Y - q.Y}
}

type Path []Point

func (path Path) Translate(offset *Point, add bool) {
	var op func(p, q *Point) *Point

	if add {
		op = (*Point).Add
	} else {
		op = (*Point).Sub
	}

	for i := range path {
		path[i] = *op(&path[i], offset)
	}
}

func main() {
	path := Path{
		{1.0, 2.0},
		{1.0, 3.0},
		{4.0, 5.0},
	}
	point := Point{
		0.3,
		0.4,
	}

	path.Translate(&point, false)

	fmt.Println(path)
}
