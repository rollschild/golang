package main

import (
	"fmt"
	"log"
	"os"

	"golang/github"
)

func main() {
	res, err := github.SearchIssues(os.Args[1:])
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Total %d issues:\n", res.TotalCount)
	for _, item := range res.Items {
		fmt.Printf("#%-5d %9.9s %.55s\n", item.Number, item.User.Login, item.Title)
		// %- left justify
		// %9.9s width & precision
	}
}
