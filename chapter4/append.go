package main

import "fmt"

func appendInt(s []int, ele int) []int {
	var res []int
	resLen := len(s) + 1 // length of the result slice

	if resLen <= cap(s) {
		res = s[:resLen]
	} else {
		// Need to allocate a new array!
		resCap := resLen
		if resCap < 2*len(s) {
			resCap = 2 * len(s)
		}

		res = make([]int, resLen, resCap)
		copy(res, s)
	}

	res[len(s)] = ele
	return res
}

func appendIntVar(s []int, ints ...int) []int {
	var res []int
	resLen := len(s) + len(ints)

	if resLen <= cap(s) {
		res = s[:resLen]
	} else {
		// Allocate new array
		resCap := resLen
		if resCap < 2*len(s) {
			resCap = 2 * len(s)
		}

		res = make([]int, resLen, resCap)
		copy(res, s)
	}

	copy(res[len(s):], ints)
	return res
}

func main() {
	var x []int

	/* for i := 0; i < 10; i++ {
	 *     y = appendInt(x, i)
	 *     fmt.Printf("%d\tcap=%d\t%v\n", i, cap(y), y)
	 *     x = y
	 * } */

	x = appendInt(x, -9)
	x = append(x, 8, 2037)
	x = appendIntVar(x, x...)
	fmt.Println(x)
}
