package main

import (
	"encoding/json"
	"fmt"
	"log"
)

// Movie type
type Movie struct {
	Title  string
	Year   int  `json:"released"` // field tag
	Color  bool `json:"color,omitempty"`
	Actors []string
}

func main() {
	movies := []Movie{
		{
			Title:  "The Irishman",
			Year:   2019,
			Color:  true,
			Actors: []string{"Robert DeNiro", "Al Pacino"},
		},
		{
			Title:  "Beirut",
			Year:   2018,
			Color:  true,
			Actors: []string{"Jon Hamm"},
		},
	}

	// data, err := json.Marshal(movies)
	data, err := json.MarshalIndent(movies, "", "	")
	if err != nil {
		log.Fatalf("JSON marshaling failed: %s", err)
	}

	fmt.Printf("%s\n", data)

	/* Unmarshaling */
	var strippedMovies []struct {
		Title string
		Year  int  `json:"released"` // remember to attach the field tag!
		Color bool `json:"color"`
	}
	if err := json.Unmarshal(data, &strippedMovies); err != nil {
		log.Fatalf("JSON unmarshaling failed: %s", err)
	}
	fmt.Println(strippedMovies)
}
