package main

import "fmt"

type treeNode struct {
	value       int
	left, right *treeNode
}

func add(node *treeNode, val int) *treeNode {
	if node == nil {
		// allocate new treeNode
		node = new(treeNode)
		node.value = val
	} else {
		if node.value < val {
			// add to right child
			node.right = add(node.right, val)
		} else {
			node.left = add(node.left, val)
		}
	}
	return node
}

func appendValue(values []int, root *treeNode) []int {
	if root != nil {
		values = appendValue(values, root.left)
		values = append(values, root.value)
		values = appendValue(values, root.right)
	}
	return values
}

// Sort a slice of integers
func Sort(values []int) []int {
	var root *treeNode

	for _, val := range values {
		root = add(root, val)
	}

	return appendValue(values[:0], root)
}

func main() {
	ints := []int{12, 3, -5, 100, 3, 2037, 6}
	// Sort(ints)
	fmt.Println(Sort(ints))

}
