package links

import (
	"fmt"
	"net/http"

	"golang.org/x/net/html"
)

// Extract gets and parses the html document and
// returns a slice of all href links
func Extract(url string) ([]string, error) {
	res, err := http.Get(url)
	if err != nil {
		res.Body.Close()
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		res.Body.Close()
		return nil, fmt.Errorf("getting %s: %s", url, res.Status)
	}

	doc, err := html.Parse(res.Body)
	if err != nil {
		res.Body.Close()
		return nil, fmt.Errorf("parsing %s as HTML: %v", url, err)
	}

	var links []string
	visitNode := func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == "a" {
			// anchor tag
			for _, a := range node.Attr {
				if a.Key != "href" {
					continue
				}
				// href
				// parse the url relative to the base url of the document
				// res.Request.URL
				// the result is an absolute form
				link, err := res.Request.URL.Parse(a.Val)
				if err != nil {
					continue // ignore bad URLs
				}
				links = append(links, link.String())
			}
		}
	}

	forEachNode(doc, visitNode, nil)
	return links, nil
}

func forEachNode(node *html.Node, pre, post func(node *html.Node)) {
	if pre != nil {
		pre(node)
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		forEachNode(c, pre, post)
	}
	if post != nil {
		post(node)
	}
}
