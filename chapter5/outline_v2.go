package main

import (
	"fmt"
	"os"

	"golang.org/x/net/html"
)

var depth int

func main() {
	doc, err := html.Parse(os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "outline_v2: %v\n", err)
	}
	forEachNode(doc, startElement, endElement)
}

func forEachNode(node *html.Node, preFunc, postFunc func(node *html.Node)) {
	if preFunc != nil {
		preFunc(node)
	}

	for c := node.FirstChild; c != nil; c = c.NextSibling {
		forEachNode(c, preFunc, postFunc)
	}

	if postFunc != nil {
		postFunc(node)
	}
}

func startElement(node *html.Node) {
	if node.Type == html.ElementNode {
		fmt.Printf("%*s<%s>\n", depth*2, "", node.Data)
		depth++
	}
}

func endElement(node *html.Node) {
	if node.Type == html.ElementNode {
		depth--
		fmt.Printf("%*s</%s>\n", depth*2, "", node.Data)
	}
}
