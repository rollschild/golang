package main

import (
	"fmt"
	"net/http"

	"golang.org/x/net/html"
)

func parse(url string) (*html.Node, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	doc, err := html.Parse(res.Body)
	if err != nil {
		return nil, fmt.Errorf("error parsing %s: %v", url, err)
	}

	return doc, nil
}

func forEachNode(node *html.Node, pre, post func(node *html.Node)) {
	if pre != nil {
		pre(node)
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		forEachNode(c, pre, post)
	}
	if post != nil {
		post(node)
	}
}

func soleTitle(doc *html.Node) (title string, err error) {
	type bailout struct{}

	defer func() {
		switch p := recover(); p {
		case nil:
			// no panic
		case bailout{}:
			err = fmt.Errorf("multiple title elements")
		default:
			panic(p)
		}
	}()

	forEachNode(doc, func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == "title" && node.FirstChild != nil {
			if title != "" {
				// already has a title
				panic(bailout{})
			}
			title = node.FirstChild.Data
		}
	}, nil)

	if title == "" {
		// no title in this html document
		return "", fmt.Errorf("no title element at all")
	}

	return title, nil
}

func main() {
	url := "https://golang.org/"
	doc, err := parse(url)
	if err != nil {
		fmt.Println(err)
	}
	title, err := soleTitle(doc)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("title is %s\n", title)
}
