package main

import (
	"fmt"
	"sort"
)

var prereqs = map[string][]string{
	"algorithms": {"data structures"},
	"calculus":   {"linear algebra"},
	"compilers": {
		"data structures",
		"formal languages",
		"computer organization",
	},
	"data structures":       {"discrete math"},
	"databases":             {"data structures"},
	"discrete math":         {"intro to programming"},
	"formal languages":      {"discrete math"},
	"networks":              {"operating systems"},
	"operating systems":     {"data structures", "computer organization"},
	"programming languages": {"data structures", "computer organization"},
}

func main() {
	for i, course := range topoSort(prereqs) {
		fmt.Printf("%d\t%s\n", i+1, course)
	}
}

func topoSort(coursesMap map[string][]string) []string {
	// depth-first search
	var courseList []string
	visited := make(map[string]bool)
	var visitAll func([]string)

	visitAll = func(items []string) {
		for _, course := range items {
			if !visited[course] {
				visited[course] = true
				visitAll(coursesMap[course])
				courseList = append(courseList, course)
			}
		}
	}

	var keys []string
	for key := range coursesMap {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	visitAll(keys)
	return courseList
}
