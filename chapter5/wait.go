package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

// WaitForServer gets an url and will retry if server not responding
func WaitForServer(url string) error {
	const timeout = 2 * time.Minute
	deadline := time.Now().Add(timeout)
	for tries := 0; time.Now().Before(deadline); tries++ {
		_, err := http.Head(url)
		if err == nil {
			return nil
		}
		log.Printf("server not responding (%s), retrying...", err)
		time.Sleep(time.Second << uint(tries))
	}

	return fmt.Errorf("server %s failed to respond after %s", url, timeout)
}

func main() {
	const website = "https://nonexistingwebsite.com"
	WaitForServer(website)
}
