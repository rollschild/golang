package main

import (
	"fmt"
	"golang/links"
	"log"
	"os"
)

func bfs(util func(item string) []string, worklist []string) {
	visited := make(map[string]bool)
	for len(worklist) > 0 {
		items := worklist
		worklist = nil
		for _, item := range items {
			if !visited[item] {
				visited[item] = true
				worklist = append(worklist, util(item)...)
			}
		}
	}
}

func crawl(url string) []string {
	fmt.Println(url)
	list, err := links.Extract(url)
	if err != nil {
		log.Print(err)
	}
	return list
}

func main() {
	bfs(crawl, os.Args[1:])
}
